 package com.example.examenc2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.examenc2.Modelo.ProductoDb;

 public class MainActivity extends AppCompatActivity {

    private Button btnGuardar, btnLimpiar, btnNuevo, btnEditar;
    private EditText txtCodigo, txtNombre, txtMarca, txtPrecio;
    private RadioButton rbtnPerecedero, rbtnNoPerecedero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {limpiarCampos();}
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {registrarProducto();}
        });

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {limpiarCampos();}
        });

        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {editar();}
        });

    }

     private void iniciarComponentes() {
         btnGuardar = findViewById(R.id.btnGuardar);
         btnEditar = findViewById(R.id.btnEditar);
         btnLimpiar = findViewById(R.id.btnLimpiar);
         btnNuevo = findViewById(R.id.btnNuevo);
         txtCodigo = findViewById(R.id.txtCodigo);
         txtNombre = findViewById(R.id.txtNombre);
         txtMarca = findViewById(R.id.txtMarca);
         txtPrecio = findViewById(R.id.txtPrecio);
         rbtnPerecedero = findViewById(R.id.rbtnPerecedero);
         rbtnNoPerecedero = findViewById(R.id.rbtnNoPerecedero);
     }

     private boolean validarCampos() {
         String codigo = txtCodigo.getText().toString();
         String nombre = txtNombre.getText().toString();
         String marca = txtMarca.getText().toString();
         String precio = txtPrecio.getText().toString();

         return !codigo.isEmpty() && !nombre.isEmpty() && !marca.isEmpty() && !precio.isEmpty();
     }

     private void registrarProducto() {
         String codigo = txtCodigo.getText().toString().trim();
         String nombre = txtNombre.getText().toString().trim();
         String marca = txtMarca.getText().toString().trim();
         String precioString = txtPrecio.getText().toString().trim();

         // Validar que los campos no estén vacíos
         if (codigo.isEmpty() || nombre.isEmpty() || marca.isEmpty() || precioString.isEmpty()) {
             mostrarToast("Por favor, ingrese todos los campos");
             return;
         }

         // Obtener el precio como double
         double precio;
         try {
             precio = Double.parseDouble(precioString);
         } catch (NumberFormatException e) {
             mostrarToast("El precio debe ser un número válido");
             return;
         }

         // Obtener el valor de esPerecedero basándote en la selección de los RadioButtons
         boolean esPerecedero = false;
         if (rbtnPerecedero.isChecked()) {
             esPerecedero = true;
         } else if (rbtnNoPerecedero.isChecked()) {
             esPerecedero = false;
         } else {
             mostrarToast("Seleccione una opción");
             return;
         }

         // Validar el valor de precio, por ejemplo:
         if (precio <= 0) {
             mostrarToast("El precio debe ser mayor que cero");
             return;
         }

         ProductoDb productoDb = new ProductoDb(MainActivity.this);
         Producto producto = new Producto(precio, codigo, nombre, marca, esPerecedero);
         long resultado = productoDb.insertProducto(producto);

         if (resultado != -1) {
             mostrarToast("Producto registrado con éxito");
         } else {
             mostrarToast("Error al registrar el producto");
         }
     }

     private void editar(){
         Intent intent = new Intent(MainActivity.this, EditarProducto.class);
         startActivity(intent);
     }

     private void mostrarToast(String mensaje) {
         Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
     }

     private void limpiarCampos() {
         txtCodigo.setText("");
         txtNombre.setText("");
         txtMarca.setText("");
         txtPrecio.setText("");
         rbtnPerecedero.setChecked(false);
         rbtnNoPerecedero.setChecked(false);
     }

 }
