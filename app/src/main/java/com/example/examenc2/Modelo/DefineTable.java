package com.example.examenc2.Modelo;

public class DefineTable {
    public DefineTable() {}
    public static abstract class Productos {
        public static final String TABLE_NAME = "productos";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_CODIGO = "codigo";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_MARCA = "marca";
        public static final String COLUMN_NAME_PRECIO = "precio";
        public static final String COLUMN_PERECEDERO = "perecedero";


        public static String[] REGISTRO = new String[]{
                COLUMN_NAME_ID,
                COLUMN_NAME_CODIGO,
                COLUMN_NAME_NOMBRE,
                COLUMN_NAME_MARCA,
                COLUMN_NAME_PRECIO,
                COLUMN_PERECEDERO
        };
    }
}