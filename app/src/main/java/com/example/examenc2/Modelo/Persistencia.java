package com.example.examenc2.Modelo;

import com.example.examenc2.Producto;

public interface Persistencia {
    void openDataBase();
    void closeDataBase();
    long insertProducto(Producto producto);
    // Agrega otros métodos de persistencia necesarios
}